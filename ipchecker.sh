#!/bin/sh

cd /home/james/repos/ip

ip2=""

ip2=$(curl ifconfig.me)

if [ -n "$ip2" ]; then

    echo "$ip2" > home-ip.txt;

    git pull
    git add home-ip.txt
    git commit -m "Updated home IP address"
    git push
    cd -
    
fi
